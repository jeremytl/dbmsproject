#include<dbconnection.h>

dbconnection::dbconnection()
{

}

bool dbconnection::opendb()
{
    database=QSqlDatabase::addDatabase("QMYSQL");

    database.setHostName("140.116.245.119");
    database.setDatabaseName("dbms");
    database.setUserName("root");
    database.setPassword("");
    if(database.open())
        return true;
    else
    {
        qDebug()<<database.lastError();
        return false;
    }
}

void dbconnection::closedb()
{
    database.close();

}

bool dbconnection::get_sp_list(QStringList &list)
{
    bool ret=false;
    QSqlQuery query("Select company_name from company");
    while(query.next())
    {
        list.append(query.value(0).toString().trimmed());
        ret=true;
    }
    return ret;
}

QSqlDatabase dbconnection::getdb()
{
    return database;
}
