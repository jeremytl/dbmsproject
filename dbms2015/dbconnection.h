#ifndef DBCONNECTION_H
#define DBCONNECTION_H
#include<QtSql/QtSql>


class dbconnection
{
public:
    dbconnection();
    bool opendb();
    void closedb();
    bool get_sp_list(QStringList &list);
    QSqlDatabase getdb();

private:
    QSqlDatabase database;

};




#endif // DBCONNECTION_H
