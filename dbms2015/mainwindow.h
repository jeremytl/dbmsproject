#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringList>
#include "dbconnection.h"
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_checkBox_clicked(bool checked);

    void on_pushButton_clicked();

    void on_radioButton_js_clicked();

    void on_radioButton_com_clicked();

    void on_select_com_combobox_currentIndexChanged(const QString &arg1);

    void on_signup_button_clicked();

    void on_query_apply_button_clicked();

    void on_comboBox_signed_bank_currentIndexChanged(const QString &arg1);

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_hbank_combobox_currentIndexChanged(const QString &arg1);

    void on_checkBox_modifymode_clicked();

    void on_checkBox_modifymode_clicked(bool checked);

    void on_tableView_activated(const QModelIndex &index);

    void on_pushButton_delete_clicked();

    void on_checkBox_js_or_vac_clicked(bool checked);

    void update_qline();

    void on_comboBox_in_currentIndexChanged(const QString &arg1);

    void on_comboBox_exist_currentIndexChanged(const QString &arg1);

    void on_checkBox_in_clicked(bool checked);

    void on_checkBox_exist_clicked(bool checked);

    void on_signup_button_clicked(bool checked);

private:
    Ui::MainWindow *ui;
    dbconnection *mydb;
    bool isjs_table,iscom_table,ishb_table,isvc_table,ispr_table;
    QString selstr,fromstr,instr,existstr;

};

#endif // MAINWINDOW_H
