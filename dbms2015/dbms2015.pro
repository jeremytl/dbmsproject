#-------------------------------------------------
#
# Project created by QtCreator 2015-12-31T16:16:49
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dbms2015
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dbconnection.cpp

HEADERS  += mainwindow.h \
    dbconnection.h

FORMS    += mainwindow.ui
