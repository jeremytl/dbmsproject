﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <fstream>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //setup all component
    mydb= new dbconnection;
    ui->setupUi(this);

    ui->hbank_combobox->setEnabled(false);
    ui->hbank_combobox->setVisible(false);
    ui->select_com_combobox->setEnabled(false);
    ui->signup_button->setEnabled(false);
    ui->checkBox->setEnabled(false);
    ui->checkBox->setVisible(false);

    isjs_table=false;
    iscom_table=false;
    ishb_table=false;
    isvc_table=false;
    ispr_table=false;

    selstr=QString("*");
    fromstr=QString("jobseeker");
    instr=QString("com_ID in (select company_ID from company where com_ID = company_ID and company_name = '"+QStringLiteral("黃心辣椒")+"')");
    existstr=QString("exists (select * from company where excom_ID = company_ID)");


    if(mydb->opendb())
    {
        qDebug()<<"GOOOD";

    }
    else
        qDebug()<<"NOOO";

    QSqlQuery *qry3=new QSqlQuery(mydb->getdb());

    QStringList banklist;

    qry3->prepare("SELECT BankID,BName from hbank");
    qry3->exec();
    qry3->next();
    ui->label_signed_bank_id->setText(qry3->value(0).toString());
    banklist.append(qry3->value(1).toString().trimmed());
    while(qry3->next())
    {
        banklist.append(qry3->value(1).toString().trimmed());
    }
    ui->comboBox_signed_bank->addItems(banklist);
    qDebug()<<banklist.value(0);

    QSqlQuery *qry=new QSqlQuery(mydb->getdb());

    QStringList inlist;

    qry->prepare("SELECT company_name from company");
    qry->exec();
    while(qry->next())
    {
        inlist.append(qry->value(0).toString().trimmed());
    }
    ui->comboBox_in->addItems(inlist);

    QStringList existlist;
    existlist<<QStringLiteral("有前公司")<<QStringLiteral("目前有公司");
    ui->comboBox_exist->addItems(existlist);
}

MainWindow::~MainWindow()
{
    mydb->closedb();
    delete ui;
    delete mydb;
}

void MainWindow::on_checkBox_clicked(bool checked)
{

}



void MainWindow::on_radioButton_js_clicked()//sign up jobseeker
{
    ui->select_com_combobox->setEnabled(false);
    ui->signup_button->setEnabled(true);
    ui->signup_button->setText(QStringLiteral("登記求職人"));
    ui->label_ini->setText(QStringLiteral("ID"));
    ui->label_in1->setText(QStringLiteral("姓名"));
    ui->label_in2->setText(QStringLiteral("年齡"));
    ui->label_in3->setText(QStringLiteral("最低薪"));
    ui->label_in4->setText(QStringLiteral("經歷(年)"));
    ui->label_in5->setText(QStringLiteral("工作類型"));
    ui->label_in6->setText(QStringLiteral("公司編號"));
    ui->label_in7->setText(QStringLiteral("前公司編號"));
    ui->lineEdit_i->setText("");
    ui->lineEdit_i->setEnabled(true);
    ui->lineEdit->setText("");
    ui->lineEdit->setEnabled(true);
    ui->lineEdit_2->setText("");
    ui->lineEdit_2->setEnabled(true);
    ui->lineEdit_3->setText("");
    ui->lineEdit_3->setEnabled(true);
    ui->lineEdit_4->setText("");
    ui->lineEdit_4->setEnabled(true);
    ui->lineEdit_5->setText("");
    ui->lineEdit_5->setEnabled(true);
    ui->lineEdit_6->setText("");
    ui->lineEdit_6->setEnabled(true);
    ui->lineEdit_7->setText("");
    ui->lineEdit_7->setEnabled(true);


    ui->lineEdit_2->setEnabled(true);
    ui->lineEdit_4->setEnabled(true);
    ui->lineEdit_7->setEnabled(true);




}

void MainWindow::on_radioButton_com_clicked()//sign up vacancy
{
    ui->select_com_combobox->setEnabled(true);
    ui->signup_button->setEnabled(true);
    ui->signup_button->setText(QStringLiteral("登記職缺"));
    ui->label_ini->setText(QStringLiteral("職缺ID"));
    ui->label_in1->setText(QStringLiteral("公司名"));
    ui->label_in2->setText(QStringLiteral(""));
    ui->label_in3->setText(QStringLiteral("薪水"));
    ui->label_in4->setText(QStringLiteral(""));
    ui->label_in5->setText(QStringLiteral("工作類型"));
    ui->label_in6->setText(QStringLiteral("公司編號"));
    ui->label_in7->setText(QStringLiteral(""));




    QSqlQuery *qry=new QSqlQuery(mydb->getdb());

    QStringList comlist;

    qry->prepare("SELECT company_name from company");
    qry->exec();
    ui->query_input->setText("SELECT company_name from company");
    while(qry->next())
    {
        comlist.append(qry->value(0).toString().trimmed());
    }
    ui->select_com_combobox->addItems(comlist);
    qDebug()<<comlist.value(0);
    QSqlQuery *qry2=new QSqlQuery(mydb->getdb());
    qry2->prepare("SELECT company_ID,company_type,company_population from company where company_name = '"+comlist.value(0)+"'");
    qry2->exec();
    qry2->next();

    ui->label_com_id_value->setText(qry2->value(0).toString());
    ui->label_com_type_value->setText(qry2->value(1).toString());
    ui->label_com_pop_value->setText(qry2->value(2).toString());





    ui->lineEdit_i->setText("");
    ui->lineEdit_i->setEnabled(true);
    ui->lineEdit->setText(comlist.value(0));
    ui->lineEdit->setEnabled(true);
    ui->lineEdit_2->setText("");
    ui->lineEdit_2->setEnabled(false);
    ui->lineEdit_3->setText("");
    ui->lineEdit_3->setEnabled(true);
    ui->lineEdit_4->setText("");
    ui->lineEdit_4->setEnabled(false);
    ui->lineEdit_5->setText("");
    ui->lineEdit_5->setEnabled(true);
    ui->lineEdit_6->setText(qry2->value(0).toString());
    ui->lineEdit_6->setEnabled(true);
    ui->lineEdit_7->setText("");
    ui->lineEdit_7->setEnabled(false);


}

void MainWindow::on_select_com_combobox_currentIndexChanged(const QString &arg1)
{
    QString com_name = ui->select_com_combobox->currentText();

    QSqlQuery *qry2=new QSqlQuery(mydb->getdb());
    qry2->prepare("SELECT company_ID,company_type,company_population from company where company_name = '"+com_name+"'");
    qry2->exec();
    ui->query_input->setText(qry2->executedQuery());

    qry2->next();

    ui->label_com_id_value->setText(qry2->value(0).toString());
    ui->label_com_type_value->setText(qry2->value(1).toString());
    ui->label_com_pop_value->setText(qry2->value(2).toString());

    ui->lineEdit_i->setText("");
    ui->lineEdit->setText(com_name);
    ui->lineEdit_2->setText("");
    ui->lineEdit_3->setText("");
    ui->lineEdit_4->setText("");
    ui->lineEdit_5->setText("");
    ui->lineEdit_6->setText(qry2->value(0).toString());
    ui->lineEdit_7->setText("");

}

void MainWindow::on_signup_button_clicked()
{
    if(ui->checkBox_modifymode->isChecked())
    {
        QString id,name,age,sal,exp,type,com,excom,sin;
        id=ui->lineEdit_i->text();
        name=ui->lineEdit->text();
        age=ui->lineEdit_2->text();
        sal=ui->lineEdit_3->text();
        exp=ui->lineEdit_4->text();
        type=ui->lineEdit_5->text();
        com=ui->lineEdit_6->text();
        excom=ui->lineEdit_7->text();
        sin=ui->label_signed_bank_id->text();

        QSqlQuery qry;
        if(isjs_table)
            qry.prepare("update jobseeker set Name = '"+name+
                            "', age = '"+age+
                            "', proposed_lsalary = '"+sal+
                            "', exp = '"+exp+
                            "', jobtype = '"+type+
                            "', com_ID = '"+com+
                            "', excom_ID = '"+excom+
                            "', signed_ID = '"+sin+
                            "' where ID = '"+id+"'");
        else if(iscom_table)
            qry.prepare("update company set company_name = '"+name+
                            "', company_type = '"+age+
                            "', company_population = '"+sal+
                            "' where company_ID = '"+id+"'");
        else if(isvc_table)
            qry.prepare("update vacancy set vacancy_jobtype = '"+name+
                            "', vacancy_salary = '"+age+
                            "', vacancy_signed_ID = '"+sal+
                            "', vacanacy_com_ID = '"+exp+
                            "' where vacancy_ID = '"+id+"'");
        else if(ishb_table)
            qry.prepare("update hbank set BName = '"+name+
                            "', ranking = '"+age+
                            "' where BankID = '"+id+"'");
        else if(ispr_table)
            qry.prepare("update pair set pairing_date = '"+name+
                            "', pairing_person = '"+age+
                            "', pairjobtype = '"+sal+
                            "', pairjobsalary = '"+exp+
                            "', pairbank_ID = '"+type+
                            "' where pair_ID = '"+id+"'");
        else return;

        ui->query_input->setText(qry.executedQuery());

        if(qry.exec())
        {
            ui->label_remind->setText("update success!");
            qDebug()<<id;
        }
        else
        {
            ui->label_remind->setText("update failed!");
        }
    }
    else
    {
        if(ui->radioButton_js->isChecked())
        {

            QString id,name,age,sal,exp,type,com,excom,sin;
            id=ui->lineEdit_i->text();
            name=ui->lineEdit->text();
            age=ui->lineEdit_2->text();
            sal=ui->lineEdit_3->text();
            exp=ui->lineEdit_4->text();
            type=ui->lineEdit_5->text();
            com=ui->lineEdit_6->text();
            excom=ui->lineEdit_7->text();
            sin=ui->label_signed_bank_id->text();
            QSqlQuery qry;
            qry.prepare("insert into jobseeker (ID,Name,age,proposed_lsalary,exp,jobtype,com_ID,excom_ID,signed_ID) values ('"+id+"','"
                        +name+"','"+age+"','"+sal+"','"+exp+"','"+type+"','"+com+"','"+excom+"','"+sin+"')");
            ui->query_input->setText("insert into jobseeker (ID,Name,age,proposed_lsalary,exp,jobtype,com_ID,excom_ID,signed_ID) values ('"+id+"','"
                                     +name+"','"+age+"','"+sal+"','"+exp+"','"+type+"','"+com+"','"+excom+"','"+sin+"')");

            if(qry.exec())
            {
                ui->label_remind->setText("Signed up success!");
                qDebug()<<id;
            }
            else
            {
                ui->label_remind->setText("Signed up failed!");
            }
        }
        else if (ui->radioButton_com->isChecked())
        {
            QString id,sal,type,com,sin;
            id=ui->lineEdit_i->text();
    //        name=ui->lineEdit->text();
    //        age=ui->lineEdit_2->text();
            sal=ui->lineEdit_3->text();
    //        exp=ui->lineEdit_4->text();
            type=ui->lineEdit_5->text();
            com=ui->lineEdit_6->text();
    //        excom=ui->lineEdit_7->text();
            sin=ui->label_signed_bank_id->text();
            QSqlQuery qry;
            qry.prepare("insert into vacancy (vacancy_ID,vacancy_jobtype,vacancy_salary,vacancy_signed_ID,vacancy_com_ID) values ('"+id+"','"
                        +sal+"','"+type+"','"+sin+"','"+com+"')");
            ui->query_input->setText("insert into vacancy (vacancy_ID,vacancy_jobtype,vacancy_salary,vacancy_signed_ID,vacancy_com_ID) values ('"+id+"','"
                                     +sal+"','"+type+"','"+sin+"','"+com+"')");

            if(qry.exec())
            {
                ui->label_remind->setText("Signed up success!");
                qDebug()<<id;
            }
            else
            {
                ui->label_remind->setText("Signed up failed!");
            }
        }

    }

}






void MainWindow::on_query_apply_button_clicked()
{

    QSqlQueryModel *m=new QSqlQueryModel();
    QSqlQuery qry;
    qry.prepare(ui->query_input->text());
    if(qry.exec())
    {
        ui->label_remind->setText("Query execution Success!");
        //qDebug()<<id;
    }
    else
    {
        ui->label_remind->setText("Query execution Failed! Some error occurs!");
    }

    m->setQuery(qry);

    ui->tableView->setModel(m);

    qDebug()<<m->columnCount();


}



void MainWindow::on_comboBox_signed_bank_currentIndexChanged(const QString &arg1)
{
    QString com_name = ui->comboBox_signed_bank->currentText();
    QSqlQuery *qry=new QSqlQuery(mydb->getdb());
    qry->prepare("SELECT BankID from hbank where BName = '"+com_name+"'");
    qry->exec();
    qry->next();

    ui->label_signed_bank_id->setText(qry->value(0).toString());
}
/*****************************/



void MainWindow::on_pushButton_clicked()//load all jobseeker
{

    QSqlQueryModel *m=new QSqlQueryModel();

    QSqlQuery *qry=new QSqlQuery(mydb->getdb());

    qry->prepare("SELECT * from jobseeker");

    if(qry->exec())
    {
        ui->label_remind->setText("Query execution Success!");
        //qDebug()<<id;
    }
    else
    {
        ui->label_remind->setText("Query execution Failed! Some error occurs!");
    }
    ui->query_input->setText("SELECT * from jobseeker");


    m->setQuery(*qry);

    ui->tableView->setModel(m);

    qDebug()<<m->columnCount();

    if(ui->checkBox_modifymode->isChecked())
    {
        isjs_table=true;
        iscom_table=false;
        ishb_table=false;
        isvc_table=false;
        ispr_table=false;
        ui->label_ini->setText(QStringLiteral("ID"));
        ui->label_in1->setText(QStringLiteral("姓名"));
        ui->label_in2->setText(QStringLiteral("年齡"));
        ui->label_in3->setText(QStringLiteral("最低薪"));
        ui->label_in4->setText(QStringLiteral("經歷(年)"));
        ui->label_in5->setText(QStringLiteral("工作類型"));
        ui->label_in6->setText(QStringLiteral("公司編號"));
        ui->label_in7->setText(QStringLiteral("前公司編號"));
        ui->lineEdit_i->setText("");
        ui->lineEdit_i->setEnabled(true);
        ui->lineEdit->setText("");
        ui->lineEdit->setEnabled(true);
        ui->lineEdit_2->setText("");
        ui->lineEdit_2->setEnabled(true);
        ui->lineEdit_3->setText("");
        ui->lineEdit_3->setEnabled(true);
        ui->lineEdit_4->setText("");
        ui->lineEdit_4->setEnabled(true);
        ui->lineEdit_5->setText("");
        ui->lineEdit_5->setEnabled(true);
        ui->lineEdit_6->setText("");
        ui->lineEdit_6->setEnabled(true);
        ui->lineEdit_7->setText("");
        ui->lineEdit_7->setEnabled(true);
        ui->signup_button->setEnabled(true);
        ui->pushButton_delete->setEnabled(true);
    }
}

void MainWindow::on_pushButton_2_clicked()//load all company
{
    QSqlQueryModel *m=new QSqlQueryModel();

    QSqlQuery *qry=new QSqlQuery(mydb->getdb());

    qry->prepare("SELECT * from company");

    if(qry->exec())
    {
        ui->label_remind->setText("Query execution Success!");
        //qDebug()<<id;
    }
    else
    {
        ui->label_remind->setText("Query execution Failed! Some error occurs!");
    }
    ui->query_input->setText("SELECT * from company");


    m->setQuery(*qry);

    ui->tableView->setModel(m);

    qDebug()<<m->columnCount();
    if(ui->checkBox_modifymode->isChecked())
    {
        iscom_table=true;
        isjs_table=false;
        ishb_table=false;
        isvc_table=false;
        ispr_table=false;
        ui->label_ini->setText(QStringLiteral("公司編號"));
        ui->label_in1->setText(QStringLiteral("公司名"));
        ui->label_in2->setText(QStringLiteral("公司類型"));
        ui->label_in3->setText(QStringLiteral("公司人口"));
        ui->label_in4->setText(QStringLiteral(""));
        ui->label_in5->setText(QStringLiteral(""));
        ui->label_in6->setText(QStringLiteral(""));
        ui->label_in7->setText(QStringLiteral(""));
        ui->lineEdit_i->setText("");
        ui->lineEdit_i->setEnabled(true);
        ui->lineEdit->setText("");
        ui->lineEdit->setEnabled(true);
        ui->lineEdit_2->setText("");
        ui->lineEdit_2->setEnabled(true);
        ui->lineEdit_3->setText("");
        ui->lineEdit_3->setEnabled(true);
        ui->lineEdit_4->setText("");
        ui->lineEdit_4->setEnabled(false);
        ui->lineEdit_5->setText("");
        ui->lineEdit_5->setEnabled(false);
        ui->lineEdit_6->setText("");
        ui->lineEdit_6->setEnabled(false);
        ui->lineEdit_7->setText("");
        ui->lineEdit_7->setEnabled(false);
        ui->signup_button->setEnabled(true);
        ui->pushButton_delete->setEnabled(true);
    }
}

void MainWindow::on_pushButton_3_clicked()//load all vacancy
{
    QSqlQueryModel *m=new QSqlQueryModel();

    QSqlQuery *qry=new QSqlQuery(mydb->getdb());

    qry->prepare("SELECT * from vacancy");

    if(qry->exec())
    {
        ui->label_remind->setText("Query execution Success!");
        //qDebug()<<id;
    }
    else
    {
        ui->label_remind->setText("Query execution Failed! Some error occurs!");
    }
    ui->query_input->setText("SELECT * from vacancy");


    m->setQuery(*qry);

    ui->tableView->setModel(m);

    qDebug()<<m->columnCount();
    if(ui->checkBox_modifymode->isChecked())
    {
        isvc_table=true;
        isjs_table=false;
        iscom_table=false;
        ishb_table=false;
        ispr_table=false;
        ui->label_ini->setText(QStringLiteral("職缺ID"));
        ui->label_in1->setText(QStringLiteral("職缺類型"));
        ui->label_in2->setText(QStringLiteral("職缺薪水"));
        ui->label_in3->setText(QStringLiteral("登記編號"));
        ui->label_in4->setText(QStringLiteral("公司編號"));
        ui->label_in5->setText(QStringLiteral(""));
        ui->label_in6->setText(QStringLiteral(""));
        ui->label_in7->setText(QStringLiteral(""));
        ui->lineEdit_i->setText("");
        ui->lineEdit_i->setEnabled(true);
        ui->lineEdit->setText("");
        ui->lineEdit->setEnabled(true);
        ui->lineEdit_2->setText("");
        ui->lineEdit_2->setEnabled(true);
        ui->lineEdit_3->setText("");
        ui->lineEdit_3->setEnabled(true);
        ui->lineEdit_4->setText("");
        ui->lineEdit_4->setEnabled(true);
        ui->lineEdit_5->setText("");
        ui->lineEdit_5->setEnabled(false);
        ui->lineEdit_6->setText("");
        ui->lineEdit_6->setEnabled(false);
        ui->lineEdit_7->setText("");
        ui->lineEdit_7->setEnabled(false);
        ui->signup_button->setEnabled(true);
        ui->pushButton_delete->setEnabled(true);
    }
}

void MainWindow::on_pushButton_4_clicked()//load all hbank
{
    QSqlQueryModel *m=new QSqlQueryModel();

    QSqlQuery *qry=new QSqlQuery(mydb->getdb());

    qry->prepare("SELECT * from hbank");

    if(qry->exec())
    {
        ui->label_remind->setText("Query execution Success!");
        //qDebug()<<id;
    }
    else
    {
        ui->label_remind->setText("Query execution Failed! Some error occurs!");
    }
    ui->query_input->setText("SELECT * from hbank");


    m->setQuery(*qry);

    ui->tableView->setModel(m);

    qDebug()<<m->columnCount();
    if(ui->checkBox_modifymode->isChecked())
    {
        ishb_table=true;
        isjs_table=false;
        iscom_table=false;
        isvc_table=false;
        ispr_table=false;
        ui->label_ini->setText(QStringLiteral("ID"));
        ui->label_in1->setText(QStringLiteral("姓名"));
        ui->label_in2->setText(QStringLiteral("年齡"));
        ui->label_in3->setText(QStringLiteral(""));
        ui->label_in4->setText(QStringLiteral(""));
        ui->label_in5->setText(QStringLiteral(""));
        ui->label_in6->setText(QStringLiteral(""));
        ui->label_in7->setText(QStringLiteral(""));
        ui->lineEdit_i->setText("");
        ui->lineEdit_i->setEnabled(true);
        ui->lineEdit->setText("");
        ui->lineEdit->setEnabled(true);
        ui->lineEdit_2->setText("");
        ui->lineEdit_2->setEnabled(true);
        ui->lineEdit_3->setText("");
        ui->lineEdit_3->setEnabled(false);
        ui->lineEdit_4->setText("");
        ui->lineEdit_4->setEnabled(false);
        ui->lineEdit_5->setText("");
        ui->lineEdit_5->setEnabled(false);
        ui->lineEdit_6->setText("");
        ui->lineEdit_6->setEnabled(false);
        ui->lineEdit_7->setText("");
        ui->lineEdit_7->setEnabled(false);
        ui->signup_button->setEnabled(true);
        ui->pushButton_delete->setEnabled(true);
    }
}

void MainWindow::on_pushButton_5_clicked()//load all pair
{
    QSqlQueryModel *m=new QSqlQueryModel();

    QSqlQuery *qry=new QSqlQuery(mydb->getdb());

    qry->prepare("SELECT * from pair");

    if(qry->exec())
    {
        ui->label_remind->setText("Query execution Success!");
        //qDebug()<<id;
    }
    else
    {
        ui->label_remind->setText("Query execution Failed! Some error occurs!");
    }
    ui->query_input->setText("SELECT * from pair");


    m->setQuery(*qry);

    ui->tableView->setModel(m);

    qDebug()<<m->columnCount();
    if(ui->checkBox_modifymode->isChecked())
    {
        ispr_table=true;
        isjs_table=false;
        iscom_table=false;
        ishb_table=false;
        isvc_table=false;
        ui->label_ini->setText(QStringLiteral("ID"));
        ui->label_in1->setText(QStringLiteral("姓名"));
        ui->label_in2->setText(QStringLiteral("年齡"));
        ui->label_in3->setText(QStringLiteral("最低薪"));
        ui->label_in4->setText(QStringLiteral("經歷(年)"));
        ui->label_in5->setText(QStringLiteral("工作類型"));
        ui->label_in6->setText(QStringLiteral(""));
        ui->label_in7->setText(QStringLiteral(""));
        ui->lineEdit_i->setText("");
        ui->lineEdit_i->setEnabled(true);
        ui->lineEdit->setText("");
        ui->lineEdit->setEnabled(true);
        ui->lineEdit_2->setText("");
        ui->lineEdit_2->setEnabled(true);
        ui->lineEdit_3->setText("");
        ui->lineEdit_3->setEnabled(true);
        ui->lineEdit_4->setText("");
        ui->lineEdit_4->setEnabled(true);
        ui->lineEdit_5->setText("");
        ui->lineEdit_5->setEnabled(true);
        ui->lineEdit_6->setText("");
        ui->lineEdit_6->setEnabled(false);
        ui->lineEdit_7->setText("");
        ui->lineEdit_7->setEnabled(false);
        ui->signup_button->setEnabled(true);
        ui->pushButton_delete->setEnabled(true);
    }
}

void MainWindow::on_hbank_combobox_currentIndexChanged(const QString &arg1)
{

}

void MainWindow::on_checkBox_modifymode_clicked()
{

}

void MainWindow::on_checkBox_modifymode_clicked(bool checked)
{
    if(checked)
    {
        ui->select_com_combobox->setEnabled(false);
        ui->radioButton_com->setEnabled(false);
        ui->radioButton_js->setEnabled(false);
        ui->signup_button->setEnabled(false);
        ui->signup_button->setText(QStringLiteral("更新"));
        ui->pushButton_delete->setEnabled(false);
    }
    else
    {
        ui->select_com_combobox->setEnabled(true);
        ui->radioButton_com->setEnabled(true);
        ui->radioButton_js->setEnabled(true);
        ui->pushButton_delete->setEnabled(false);
        if(ui->radioButton_js->isChecked())
        {
            ui->signup_button->setEnabled(true);
            ui->signup_button->setText(QStringLiteral("登記求職人"));
        }
        else if(ui->radioButton_com->isChecked())
        {
            ui->signup_button->setEnabled(true);
            ui->signup_button->setText(QStringLiteral("登記職缺"));
        }
        else
        {
            ui->signup_button->setEnabled(false);
            ui->signup_button->setText(QStringLiteral(""));
        }
        isjs_table=false;
        iscom_table=false;
        ishb_table=false;
        isvc_table=false;
        ispr_table=false;
    }
}

void MainWindow::on_tableView_activated(const QModelIndex &index)
{
    QString tablesel=ui->tableView->model()->data(index).toString();
    qDebug()<<index.column();
    if(index.column()==0)
    {
        QSqlQuery *qry=new QSqlQuery(mydb->getdb());
        if(isjs_table)
        {
                qry->prepare("SELECT * from jobseeker where ID = '" + tablesel + "'");
                if(qry->exec())
                {
                    qry->next();
                    ui->lineEdit_i->setText(qry->value(0).toString());
                    ui->lineEdit->setText(qry->value(1).toString());
                    ui->lineEdit_2->setText(qry->value(2).toString());
                    ui->lineEdit_3->setText(qry->value(3).toString());
                    ui->lineEdit_4->setText(qry->value(4).toString());
                    ui->lineEdit_5->setText(qry->value(5).toString());
                    ui->lineEdit_6->setText(qry->value(6).toString());
                    ui->lineEdit_7->setText(qry->value(7).toString());
                    ui->label_signed_bank_id->setText(qry->value(8).toString());

                    ui->label_remind->setText("Query execution Success!");
                    qDebug()<<"YO";
                }
                else
                {
                    ui->label_remind->setText("Query execution Failed! Some error occurs!");
                }
                qDebug()<<"YO";
        }
        else if(iscom_table)
        {
                qry->prepare("SELECT * from company where company_ID = '" + tablesel + "'");
                if(qry->exec())
                {
                    qry->next();
                    ui->lineEdit_i->setText(qry->value(0).toString());
                    ui->lineEdit->setText(qry->value(1).toString());
                    ui->lineEdit_2->setText(qry->value(2).toString());
                    ui->lineEdit_3->setText(qry->value(3).toString());
                    ui->lineEdit_4->setText(qry->value(4).toString());

                    ui->label_remind->setText("Query execution Success!");
                    //qDebug()<<id;
                }
                else
                {
                    ui->label_remind->setText("Query execution Failed! Some error occurs!");
                }

        }
        else if(ishb_table)
        {
                qry->prepare("SELECT * from hbank where BankID = '" + tablesel + "'");
                if(qry->exec())
                {
                    qry->next();
                    ui->lineEdit_i->setText(qry->value(0).toString());
                    ui->lineEdit->setText(qry->value(1).toString());
                    ui->lineEdit_2->setText(qry->value(2).toString());

                    ui->label_remind->setText("Query execution Success!");
                    //qDebug()<<id;
                }
                else
                {
                    ui->label_remind->setText("Query execution Failed! Some error occurs!");
                }

        }
        else if(isvc_table)
        {
                qry->prepare("SELECT * from vacancy where vacancy_ID = '" + tablesel + "'");
                if(qry->exec())
                {
                    qry->next();
                    ui->lineEdit_i->setText(qry->value(0).toString());
                    ui->lineEdit->setText(qry->value(1).toString());
                    ui->lineEdit_2->setText(qry->value(2).toString());
                    ui->lineEdit_3->setText(qry->value(3).toString());
                    ui->lineEdit_4->setText(qry->value(4).toString());

                    ui->label_remind->setText("Query execution Success!");
                    //qDebug()<<id;
                }
                else
                {
                    ui->label_remind->setText("Query execution Failed! Some error occurs!");
                }

        }
        else if(ispr_table)
        {
                qry->prepare("SELECT * from pair where pair_ID = '" + tablesel + "'");
                if(qry->exec())
                {
                    qry->next();
                    ui->lineEdit_i->setText(qry->value(0).toString());
                    ui->lineEdit->setText(qry->value(1).toString());
                    ui->lineEdit_2->setText(qry->value(2).toString());
                    ui->lineEdit_3->setText(qry->value(3).toString());
                    ui->lineEdit_4->setText(qry->value(4).toString());
                    ui->lineEdit_5->setText(qry->value(5).toString());

                    ui->label_remind->setText("Query execution Success!");
                    //qDebug()<<id;
                }
                else
                {
                    ui->label_remind->setText("Query execution Failed! Some error occurs!");
                }

        }

    }

}

void MainWindow::on_pushButton_delete_clicked() //delete info
{
    QString id;
    id=ui->lineEdit_i->text();
    QSqlQuery qry;
    if(isjs_table)
        qry.prepare("delete from jobseeker where ID = '"+id+"'");
    else if(iscom_table)
        qry.prepare("delete from company where company_ID = '"+id+"'");
    else if(isvc_table)
        qry.prepare("delete from vacancy where vacancy_ID = '"+id+"'");
    else if(ishb_table)
        qry.prepare("delete from hbank where BankID = '"+id+"'");
    else if(ispr_table)
        qry.prepare("delete from pair where pair_ID = '"+id+"'");
    else return;

    ui->query_input->setText(qry.executedQuery());

    if(qry.exec())
    {
        ui->label_remind->setText("delete success!");
        qDebug()<<id;
    }
    else
    {
        ui->label_remind->setText("delete failed!");
    }
}

void MainWindow::on_checkBox_js_or_vac_clicked(bool checked)
{
    if(checked)
    {
        fromstr = QString("jobseeker");
    }
    else
    {
        fromstr = QString("vacancy");
    }
    update_qline();
}

void MainWindow::update_qline()
{
    ui->query_input->setText("select "+selstr+" from "+fromstr);
    if(ui->checkBox_in->isChecked())
    {
        ui->query_input->setText("select "+selstr+" from "+fromstr + " where "+instr);
    }
    else if(ui->checkBox_exist->isChecked())
    {
        ui->query_input->setText("select "+selstr+" from "+fromstr + " where "+existstr);
    }

}
void MainWindow::on_checkBox_in_clicked(bool checked)
{
    if(checked)
    {
        instr=QString("com_ID in (select company_ID from company where com_ID = company_ID and company_name = '"+ui->comboBox_in->currentText()+"')");
    }
    update_qline();

}
void MainWindow::on_comboBox_in_currentIndexChanged(const QString &arg1)
{
    if(ui->checkBox_in->isChecked())
        instr=QString("com_ID in (select company_ID from company where com_ID = company_ID and company_name = '"+ui->comboBox_in->currentText()+"')");
    update_qline();
}
void MainWindow::on_checkBox_exist_clicked(bool checked)
{
    if(checked)
    {
        ui->query_input->setText("select * from jobseeker");
    }
    update_qline();
}
void MainWindow::on_comboBox_exist_currentIndexChanged(const QString &arg1)
{
    if(ui->checkBox_exist->isChecked())
    {
    if(ui->comboBox_exist->currentIndex()==0)
        existstr=QString("exists (select * from company where excom_ID = company_ID)");
    else
        existstr=QString("exists (select * from company where com_ID = company_ID)");
    }
    update_qline();
}




